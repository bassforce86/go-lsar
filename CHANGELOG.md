# CHANGELOG

<!--- next entry here -->

## 0.1.0
2021-02-11

### Features

- added tests
- removed dependency on external events package
- switched single error returns for error collection returns to allow for more verbose error handling

### Fixes

- added release stage to ci
