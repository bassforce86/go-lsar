package lsar

import (
	"fmt"
	"reflect"
	"testing"
)

func TestDefaultOptions(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want ArchiveOptions
	}{
		{
			name: "Should set AsJSON, List & RunTest by default",
			args: args{path: "test/file.zip"},
			want: ArchiveOptions{
				Path:    "test/file.zip",
				AsJSON:  true,
				List:    true,
				RunTest: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultOptions(tt.args.path); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DefaultOptions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetArchiveContents(t *testing.T) {
	type args struct {
		opts ArchiveOptions
	}
	tests := []struct {
		name    string
		args    args
		want    ArchiveInfo
		wantErr bool
	}{
		{
			name: "should return the contents of an archive",
			args: args{
				opts: ArchiveOptions{
					Path:    "/test_files/test.zip",
					List:    true,
					AsJSON:  true,
					RunTest: true,
				},
			},
			want: ArchiveInfo{
				FormatVersion: 2,
				Confidence:    0,
				Properties: ArchiveProperties{
					IsCorrupted:      0,
					IsSolid:          0,
					VolumeScanFailed: 0,
					Volumes:          []string(nil),
				},
				Contents: []ArchiveContents(nil),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetArchiveContents(tt.args.opts)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetArchiveContents() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetArchiveContents() = %+v, want %+v", got, tt.want)
			}
		})
	}
}

func Test_parseOptions(t *testing.T) {
	type args struct {
		o ArchiveOptions
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{name: "Should return empty options", args: args{o: ArchiveOptions{}}, want: []string(nil)},
		{name: "Should return -l", args: args{o: ArchiveOptions{List: true}}, want: []string{"-l"}},
		{name: "Should return -j", args: args{o: ArchiveOptions{AsJSON: true}}, want: []string{"-j"}},
		{name: "Should return -t", args: args{o: ArchiveOptions{RunTest: true}}, want: []string{"-t"}},
		{name: "Should return -l, -j, -t", args: args{o: ArchiveOptions{
			List: true, AsJSON: true, RunTest: true,
		}}, want: []string{"-l", "-j", "-t"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseOptions(tt.args.o); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseOptions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_commandExists(t *testing.T) {
	type args struct {
		cmd string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "should return true if command is available",
			args: args{cmd: "go"},
			want: true,
		},
		{
			name: "should return false if this_is_not_a_valid_command is not available",
			args: args{cmd: "this_is_not_a_valid_command"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := commandExists(tt.args.cmd); got != tt.want {
				t.Errorf("commandExists() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestArchiveErrors_Error(t *testing.T) {
	tests := []struct {
		name string
		ae   ArchiveErrors
		want string
	}{
		{
			name: "Should return a single error",
			ae: ArchiveErrors{
				ArchiveError{
					In:     "Test",
					Detail: "a single error",
				},
			},
			want: "[Test] a single error",
		},
		{
			name: "Should return multiple errors",
			ae: ArchiveErrors{
				ArchiveError{
					In:     "Test",
					Detail: "a single error",
				},
				ArchiveError{
					In:     "TestTwo",
					Detail: "another single error",
				},
			},
			want: "[Test] a single error\n[TestTwo] another single error",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ae.Error(); got != tt.want {
				t.Errorf("ArchiveErrors.Error() = %v | want: %v", got, tt.want)
			}
		})
	}
}

func TestArchiveInfo_errorCheck(t *testing.T) {
	type fields struct {
		FormatVersion int64
		FormatName    string
		Encoding      string
		Confidence    float64
		Properties    ArchiveProperties
		Contents      []ArchiveContents
	}
	type args struct {
		err error
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should return early if an error is passed in",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties:    ArchiveProperties{},
				Contents:      []ArchiveContents(nil),
			},
			args: args{
				err: fmt.Errorf("test error"),
			},
			wantErr: true,
		}, {
			name: "should return early if the Volume Scan fails",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties: ArchiveProperties{
					VolumeScanFailed: 1,
				},
				Contents: []ArchiveContents(nil),
			},
			args:    args{},
			wantErr: true,
		}, {
			name: "should return early if the Volume Scan fails",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties: ArchiveProperties{
					IsCorrupted: 1,
				},
				Contents: []ArchiveContents(nil),
			},
			args:    args{},
			wantErr: true,
		}, {
			name: "should generate and error if a file in the archive is corrupted",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties:    ArchiveProperties{},
				Contents: []ArchiveContents{
					{IsCorrupted: 1, TestResult: "ok"},
				},
			},
			args:    args{},
			wantErr: true,
		}, {
			name: "should generate and error if a file in the archive fails testing",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties:    ArchiveProperties{},
				Contents: []ArchiveContents{
					{TestResult: "wrong_checksum"},
				},
			},
			args:    args{},
			wantErr: true,
		}, {
			name: "should ignore file test if it's a directory",
			fields: fields{
				FormatVersion: 2,
				FormatName:    "Zip",
				Encoding:      "zip",
				Confidence:    0,
				Properties:    ArchiveProperties{},
				Contents: []ArchiveContents{
					{IsDirectory: 1, TestResult: "wrong_checksum"},
				},
			},
			args:    args{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ai := &ArchiveInfo{
				FormatVersion: tt.fields.FormatVersion,
				FormatName:    tt.fields.FormatName,
				Encoding:      tt.fields.Encoding,
				Confidence:    tt.fields.Confidence,
				Properties:    tt.fields.Properties,
				Contents:      tt.fields.Contents,
			}
			if err := ai.errorCheck(tt.args.err); (err != nil) != tt.wantErr {
				t.Errorf("ArchiveInfo.errorCheck() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
