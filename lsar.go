package lsar

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"strings"
)

// ArchiveOptions options to build archive
type ArchiveOptions struct {
	Path    string
	List    bool
	AsJSON  bool
	RunTest bool
}

// ArchiveInfo is the base information from the extracted archive
type ArchiveInfo struct {
	FormatVersion int64             `json:"lsarFormatVersion"`
	FormatName    string            `json:"lsarFormatName"`
	Encoding      string            `json:"lsarEncoding"`
	Confidence    float64           `json:"lsarConfidence"`
	Properties    ArchiveProperties `json:"lsarProperties"`
	Contents      []ArchiveContents `json:"lsarContents"`
}

// ArchiveContents is lsar's understanding of the archive contents
type ArchiveContents struct {
	Attributes        int64  `json:"RAR5Attributes"`
	DataLength        int64  `json:"RAR5DataLength"`
	CompressionMethod int64  `json:"RAR5CompressionMethod"`
	DictionarySize    int64  `json:"RAR5DictionarySize"`
	Index             int64  `json:"XADIndex"`
	LastModified      string `json:"XADLastModificationDate"`
	IsSolid           int64  `json:"XADIsSolid"`
	SolidLength       int64  `json:"XADSolidLength"`
	IsCorrupted       int64  `json:"XADIsCorrupted"`
	IsDirectory       int64  `json:"XADIsDirectory"`
	FileSize          int64  `json:"XADFileSize"`
	FileName          string `json:"XADFileName"`
	Flags             int64  `json:"RAR5Flags"`
	DataOffset        int64  `json:"RAR5DataOffset"`
	CompressedSize    int64  `json:"XADCompressedSize"`
	CompressionName   string `json:"XADCompressionName"`
	TestResult        string `json:"lsarTestResult"`
}

// ArchiveProperties contains the properties of an archive
type ArchiveProperties struct {
	ArchiveName      string   `json:"XADArchiveName"`
	IsCorrupted      int64    `json:"XADIsCorrupted"`
	IsSolid          int64    `json:"XADIsSolid"`
	VolumeScanFailed int64    `json:"XADVolumeScanningFailed"`
	Volumes          []string `json:"XADVolumes"`
}

// ArchiveError captures a single error in reading the archive
type ArchiveError struct {
	In     string
	Detail string
}

// ArchiveErrors is a slice of errors that are returned should multiple errors occur
type ArchiveErrors []ArchiveError

func (ae ArchiveErrors) Error() string {
	var e []string
	for _, v := range ae {
		e = append(e, fmt.Sprintf("[%s] %s", v.In, v.Detail))
	}

	return strings.Join(e, "\n")
}

// DefaultOptions returns the Default set of options we use with lsar
func DefaultOptions(path string) ArchiveOptions {
	return ArchiveOptions{
		Path:    path,
		AsJSON:  true,
		List:    true,
		RunTest: true,
	}
}

func parseOptions(o ArchiveOptions) []string {
	var options []string
	if o.List {
		options = append(options, "-l")
	}
	if o.AsJSON {
		options = append(options, "-j")
	}
	if o.RunTest {
		options = append(options, "-t")
	}
	return options
}

// GetArchiveContents return a list of archive contents
func GetArchiveContents(opts ArchiveOptions) (ArchiveInfo, ArchiveErrors) {
	var archive ArchiveInfo

	if !commandExists("lsar") {
		return archive, ArchiveErrors{ArchiveError{In: "commandExists", Detail: "lsar not installed or no available alias present"}}
	}

	options := parseOptions(opts)

	result := exec.Command("sh", "-c", fmt.Sprintf("lsar %v \"%s\"", strings.Join(options, " "), opts.Path))
	output, err := result.CombinedOutput()

	err = json.Unmarshal(output, &archive)

	return archive, archive.errorCheck(err)
}

func commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

// errorCheck Check whether lsar reported any corruption
func (ai *ArchiveInfo) errorCheck(err error) ArchiveErrors {
	if err != nil {
		return ArchiveErrors{ArchiveError{In: "errorCheck", Detail: err.Error()}}
	}

	if ai.Properties.VolumeScanFailed > 0 {
		return ArchiveErrors{ArchiveError{In: "errorCheck", Detail: "Archive Scan Failed"}}
	}

	if ai.Properties.IsCorrupted > 0 {
		return ArchiveErrors{ArchiveError{In: "errorCheck", Detail: "Archive Corrupted"}}
	}

	var ae ArchiveErrors

	for _, v := range ai.Contents {
		if v.IsCorrupted > 0 {
			ae = append(ae, ArchiveError{In: "errorCheck", Detail: "File Corrupted"})
		}
		// lsar doesn't test directories - no point
		if v.IsDirectory == 0 && v.TestResult != "ok" {
			ae = append(ae, ArchiveError{In: "errorCheck", Detail: fmt.Sprintf("Test result: %s", v.TestResult)})
		}
	}

	return ae
}
